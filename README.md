# Summary

# License
All software containted in this repository is licensed under the MIT license found in [..\License\MIT License.rtf](https://bitbucket.org/ChrisCilino/sequencer-pattern/src/master/License/MIT%20License.rtf). 


# About the Author

|       |      |
| --------|---------|
| ![PetranWay](https://partners.ni.com/partner_content/30/1201930/logo300x300.png?t=8d81dffd311d077) | I believe in good software engineering and healthy team builiding. I founded [PetranWay](https://www.petranway.com) to help software organizations grow thier software expertise, nurture team culture, and build infrastructure so that they produce quality products that meet business objectives. For more information check out [PetranWay](https://www.petranway.com).|


Please feel free to check out 

* [My Online Presentations](http://bit.ly/ChrisCilino_Presentations)
* [My Free and Open Source Code](http://bit.ly/ChrisCilino_CSuite)
* [My LinkedIn Profile](http://bit.ly/ChrisCilino_LinkedIn)
* [PetranWay](https://www.petranway.com)